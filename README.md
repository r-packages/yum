
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" style="height:250px" /> yum 📦

## ‘YAML’ Utilities & More

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/yum)](https://cran.r-project.org/package=yum)

[![Dependency
status](https://tinyverse.netlify.com/badge/yum)](https://CRAN.R-project.org/package=yum)

[![Pipeline
status](https://gitlab.com/r-packages/yum/badges/main/pipeline.svg)](https://gitlab.com/r-packages/yum/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/yum?color=brightgreen)](https://cran.r-project.org/package=yum)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/yum?color=brightgreen)](https://cran.r-project.org/package=yum)

[![Coverage
status](https://codecov.io/gl/r-packages/yum/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/yum?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://yum.opens.science>. If the development version also has a
pkgdown website, that’s located at <https://r-packages.gitlab.io/yum>.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `yum` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('yum');
```

You can install the development version of `yum` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/yum');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `yum` from [GitLab](https://about.gitlab.com)
with:

``` r
remotes::install_gitlab('r-packages/yum@dev');
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

`yum` was created to have minimal dependencies. It requires `yaml` to be
able to actually load (parse) the extracted YAML fragments, and you will
often want to have `data.tree` available to organise the results in a
tree if they have a hierarchical structure. Therefore, `yum` does have
some dependencies through those two suggested packages. Of these, `yaml`
only has one dependency, but `data.tree` has quite a few more.
<!-- Specifically, the dependency network looks like this: -->

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
